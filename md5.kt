
import java.math.BigInteger
import java.security.MessageDigest

fun MD5(value: String): String {
    val md = MessageDigest.getInstance("MD5")
    md.reset()
    md.update(value.toByteArray())
    val buf = StringBuilder()
    buf.append(BigInteger(1, md.digest()).toString(16))
    while (buf.length < 32) {
        buf.insert(0, '0')
    }
    return buf.toString().toLowerCase()
}

interface StringTransformer {
    fun transform(input: String): String
}

class DefaultStringTransformer : StringTransformer {
    override fun transform(input: String): String {
        return input
    }
}

class UpperStringTransformer : StringTransformer {
    override fun transform(input: String): String {
        return input.toUpperCase()
    }
}

fun main(args: Array<String>) {
    val values = mutableSetOf<String>()
    var stringTransformer: StringTransformer = DefaultStringTransformer()
    for (arg in args) {
        if (arg == "-u") {
            stringTransformer = UpperStringTransformer()
        } else {
            values.add(arg)
        }
    }

    for (arg in values) {
        try {
            println("$arg -> ${stringTransformer.transform(MD5(arg))}")
        } catch (err: Throwable) {
            println(err.toString())
        }
    }
}
