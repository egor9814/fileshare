
function $(id) {
    return document.getElementById(id);
}

function uploadFile() {
    var file = $("file").files[0];
    var data = new FormData();
    data.append("file", file)
    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", onProgress, false);
    ajax.addEventListener("load", onLoad, false);
    ajax.addEventListener("error", onError, false);
    ajax.addEventListener("abort", onAbort, false);
    ajax.open("POST", "/upload", true);
    ajax.setRequestHeader("X-FILENAME", file.name);
    ajax.send(data);
}

function onProgress(e) {
    $("loaded_n_total").innerHTML = "Uploaded " + e.loaded + " bytes of " + e.total;
    var pct = (e.loaded / e.total) * 100;
    $("progress_bar").value = Math.round(pct);
    $("status").innerHTML = Math.round(pct) + "% uploaded... please wait";
}

function onLoad(e) {
    window.location.replace(e.target.responseText);
    $("progress_bar").value = 0;
}

function onError(e) {
    $("status").innerHTML = "Upload failed";
}

function onAbort(e) {
    $("status").innerHTML = "Upload aborted";
}
