package util

import fileshare.InternalServerException
import java.math.BigInteger
import java.security.MessageDigest

fun MD5(value: String) = try {
    val md = MessageDigest.getInstance("MD5")
    md.reset()
    md.update(value.toByteArray())
    val buf = StringBuilder()
    buf.append(BigInteger(1, md.digest()).toString(16))
    while (buf.length < 32) {
        buf.insert(0, '0')
    }
    buf.toString().toLowerCase()
} catch (err: Throwable) {
    throw InternalServerException(err)
}

object PasswordManager {

    fun encode(password: String) = MD5(password)

}
