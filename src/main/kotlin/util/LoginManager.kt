package util

import fileshare.FSSession
import fileshare.InvalidCredentialsException
import fileshare.data.UserInfo
import fileshare.data.getUser

/*import java.util.*
import javax.mail.*
import javax.mail.internet.*
import javax.activation.**/

object LoginManager {

    fun checkSession(s: FSSession) {
        if (s.userId == null)
            throw InvalidCredentialsException("User not login", 2)
        val user = getUser(s.userId) ?: throw InvalidCredentialsException("User not found", 2)
        if (user.fullName != s.name)
            throw InvalidCredentialsException("Invalid user", 2)
    }

    fun isLogined(s: FSSession) = when (s.userId) {
        null -> false
        else -> try {
            checkUser(s)
            true
        } catch (err: Throwable) {
            false
        }
    }

    fun checkUser(s: FSSession) {
        if (s.userId != null) {
            require(s.salt != null)
            val userInfo = getUser(s.userId)!!
            val userHash = MD5(s.userId.toString()) + userInfo.nick
            val saltHash = MD5(MD5(userHash) + userInfo.pass)
            if (saltHash != s.salt)
                throw InvalidCredentialsException("Invalid user", 2)
        }
    }

    fun login(s: FSSession, userInfo: UserInfo): FSSession {
        val userHash = MD5(userInfo.id.toString()) + userInfo.nick
        val saltHash = MD5(MD5(userHash) + userInfo.pass)
        return s.copy(
            name = userInfo.fullName,
            userId = userInfo.id,
            salt = saltHash
        )
    }

    /*fun sendRegisterToken(email: String, data: String): String {
        val p = System.getProperties()
        //p.setProperty("mail.smtp.host", "localhost")
        val s = Session.getDefaultInstance(p)

        try {
            val message = MimeMessage(s)
            message.setFrom(InternetAddress("noreply.fileshare@gmail.com"))
            message.addRecipient(Message.RecipientType.TO, InternetAddress(email))
            message.subject = "FileShare Registration"
            message.description = "Registration token"
            val token = MD5(MD5(email) + MD5(data))
            message.setText("Copy this token and paste to registration form> $token")
            Transport.send(message)
            return token
        } catch (err: Throwable) {
            throw InternalServerException(err)
        }
    }*/

}
