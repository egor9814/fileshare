package util

import kotlin.math.roundToInt

enum class FileSizeUnit(prefix: String) {
    Bytes(""), KBytes("K"), MBytes("M"), GBytes("G"), TBytes("T");

    val unit by lazy { prefix + "B" }

    override fun toString(): String {
        return unit
    }
}

class FileSize(value: Double, unit: FileSizeUnit = FileSizeUnit.Bytes) {
    constructor(size: Long) : this(size.toDouble()) {
        autoDetectUnit()
    }

    var value = value
        private set

    private var mUnit = unit
    var unit: FileSizeUnit
        get() = mUnit
        set(value) {
            //val tmp = FileSize(this.value, mUnit)
            while (value > mUnit) {
                upUnit()
            }
            while (value < mUnit) {
                downUnit()
            }
        }

    fun upUnit() = apply {
        if (mUnit < FileSizeUnit.TBytes) {
            value /= 1024.0
            mUnit = FileSizeUnit.values()[mUnit.ordinal + 1]
        }
    }

    fun downUnit() = apply {
        if (mUnit > FileSizeUnit.Bytes) {
            value *= 1024.0
            mUnit = FileSizeUnit.values()[mUnit.ordinal - 1]
        }
    }

    fun autoDetectUnit() = apply {
        while (value >= 1024.0 && unit < FileSizeUnit.TBytes) {
            upUnit()
        }
        while (value <= 1.0 && unit > FileSizeUnit.Bytes) {
            downUnit()
        }
    }

    override fun toString(): String {
        return "${value.roundToInt()} $unit"
    }

    val bytes: Long
        get() = FileSize(value, unit).apply {
            unit = FileSizeUnit.Bytes
        }.value.toLong()

}
