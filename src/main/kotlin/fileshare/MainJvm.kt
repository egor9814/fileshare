package fileshare

import fileshare.pages.*
import fileshare.data.*
import io.ktor.application.*
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CORS
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.html.*
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.*
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.response.respondTextWriter
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.sessions.*
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.Color
import kotlinx.css.LinearDimension
import kotlinx.css.TextAlign
import kotlinx.css.margin
import kotlinx.css.px
import kotlinx.css.properties.TextDecoration
import kotlinx.html.*
import kotlinx.html.dom.document
import java.io.*
import util.*


class InternalServerException(val error: Throwable) : RuntimeException(error)

class InvalidCredentialsException(message: String, val code: Int) : RuntimeException(message)


fun main(args: Array<String>) {
    var debug = false
    for (arg in args) {
        if (arg == "-d" || arg == "--debug") {
            debug = true
        }
    }
    val d = Debugger(debug)

    Tables.apply {
        if (!connect())
            throw RuntimeException("cannot create DB")
        if (isEmpty()) {
            create()
            fill()
        }
    }

    embeddedServer(Netty, port = 8080) {
        install(DefaultHeaders)
        install(AutoHeadResponse)
        install(Sessions) {
            cookie<FSSession>("EA0882447EC4DBECE61497D8F24C91A3") {}
        }
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Put)
            method(HttpMethod.Delete)
            method(HttpMethod.Patch)
            header(HttpHeaders.Authorization)
            allowCredentials = true
            anyHost()
        }
        install(StatusPages) {
            exception<InvalidCredentialsException> { exception ->
                call.respondHtml {
                    defaultHead("FileShare Error!") {}
                    body {
                        titleBar {
                            backIcon()
                            title("Error")
                            icon("home", "/", TitleBar.Gravity.End)
                        }
                        card {
                            p {
                                +"Error: ${exception.message}"
                            }
                            when (exception.code) {
                                0 -> {
                                    p {
                                        a(href = "/register") { +"Sign Up" }
                                    }
                                }
                                2 -> {
                                    p {
                                        a(href = "/login") { +"Sign In" }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            exception<InternalServerException> { exception ->
                call.respondHtml {
                    defaultHead("FileShare Server Error!") {}
                    body {
                        titleBar {
                            backIcon()
                            title("Server Error")
                            icon("home", "/", TitleBar.Gravity.End)
                        }
                        card {
                            p {
                                +"Error: ${exception.cause?.message ?: exception.message}"
                            }
                            p {
                                +"Internal server error: send your data to egor.chalyh.98@mail.ru"
                            }
                            p {
                                +("Netty Engine Server on ${System.getProperty("os.name")}" +
                                        "(${System.getProperty("os.arch")}), ${System.getProperty("java.vm.name")}")
                            }
                        }
                    }
                }
            }
        }

        routing {
            Home      {this} [d]
            Login     {this} [d]
            Register  {this} [d]
            Logout    {this} [d]
            Files     {this} [d]
            FilePages {this} [d]
            User      {this} [d]
            Users     {this} [d]
            Search    {this} [d]
            Upload    {this} [d]
            BugReport {this} [d]

            static("/static") {
                resources("js")
            }
        }
    }.start(wait = true)
}

fun PipelineContext<Unit, ApplicationCall>.getSession() = call.sessions.get<FSSession>() ?: FSSession()
