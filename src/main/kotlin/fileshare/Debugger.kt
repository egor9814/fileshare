package fileshare

class Debugger(private val debug: Boolean) {

    fun <T> print(value: T?) = apply {
        if (debug)
            kotlin.io.print(value.toString())
    }

    fun <T> println(value: T?) = apply {
        if (debug)
            kotlin.io.println(value.toString())
    }

    fun println() = apply {
        if (debug)
            kotlin.io.println()
    }

}
