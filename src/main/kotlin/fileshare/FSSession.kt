package fileshare

import fileshare.data.getFileInfo
import fileshare.data.getUser

data class FSSession(
    val name: String? = null,
    val userId: Int? = null,
    val salt: String? = null
)


enum class AccessMode {
    Read, Write, Admin
}

fun FSSession.getFileAccessMode(fileId: String): Set<AccessMode> {
    val result = mutableSetOf(AccessMode.Read)
    val info = getFileInfo(fileId)
    if (userId != null) {
        if (info?.userId == userId)
            result.add(AccessMode.Write)
    }
    return result
}

fun FSSession.getAccessMode(id: Int?): Set<AccessMode> {
    val result = mutableSetOf(AccessMode.Read)
    if (userId != null) {
        if (userId == id)
            result.add(AccessMode.Write)
        if (userId == 1)
            result.add(AccessMode.Admin)
    }
    return result
}
