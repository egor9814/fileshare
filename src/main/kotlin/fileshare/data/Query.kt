package fileshare.data

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import util.FileSize
import util.MD5

fun getFileBlob(id: String): ExposedBlob {
    return Tables.transaction {
        FilesTable.select {
            FilesTable.id like id
        }.first()[FilesTable.data]
    }
}

data class FileInfo(
    val id: String,
    val name: String,
    val userId: Int,
    val creationTimestamp: Long,
    val size: FileSize,
    val type: String
)

fun getFiles(name: String): List<FileInfo> {
    return Tables.transaction {
        FilesInfoTable.select {
            FilesInfoTable.name like name
        }.map {
            FileInfo(
                it[FilesInfoTable.id],
                it[FilesInfoTable.name],
                it[FilesInfoTable.userId],
                it[FilesInfoTable.creationTimestamp],
                FileSize(it[FilesInfoTable.size]),
                it[FilesInfoTable.type]
            )
        }
    }
}

fun getFiles(userId: Int) = Tables.transaction {
    FilesInfoTable.select {
        FilesInfoTable.userId eq userId
    }.map {
        FileInfo(
            it[FilesInfoTable.id],
            it[FilesInfoTable.name],
            it[FilesInfoTable.userId],
            it[FilesInfoTable.creationTimestamp],
            FileSize(it[FilesInfoTable.size]),
            it[FilesInfoTable.type]
        )
    }
}

fun getFileInfo(fileId: String): FileInfo? {
    return Tables.transaction {
        FilesInfoTable.select {
            FilesInfoTable.id like fileId
        }.let {  q ->
            if (q.empty()) {
                null
            } else {
                q.first().let {
                    FileInfo(
                        it[FilesInfoTable.id],
                        it[FilesInfoTable.name],
                        it[FilesInfoTable.userId],
                        it[FilesInfoTable.creationTimestamp],
                        FileSize(it[FilesInfoTable.size]),
                        it[FilesInfoTable.type]
                    )
                }
            }
        }
    }
}

fun pushFile(info: FileInfo, blob: ExposedBlob) = Tables.transaction {
    FilesTable.insert {
        it[id] = info.id
        it[data] = blob
    }
    FilesInfoTable.insert {
        it[id] = info.id
        it[name] = info.name
        it[userId] = info.userId
        it[creationTimestamp] = info.creationTimestamp
        it[size] = info.size.bytes
        it[type] = info.type
    }
}

fun createFileInfo(userId: Int, fileName: String, bytes: List<Byte>): FileInfo {
    val ts = System.currentTimeMillis()

    var id = MD5(bytes.hashCode().toString())
    id = MD5(id + bytes.size.hashCode().toString())
    id = MD5(id + fileName.hashCode().toString())

    return FileInfo(id, fileName, userId, ts, FileSize(bytes.size.toLong()), "")
}

fun removeFile(info: FileInfo) = Tables.transaction {
    FilesInfoTable.deleteWhere {
        FilesInfoTable.id like info.id
    }
    FilesTable.deleteWhere {
        FilesTable.id like info.id
    }
}


fun tryLogin(userName: String, password: String): Int? {
    return Tables.transaction {
        UsersTable.select {
            (UsersTable.nick like userName) and (UsersTable.pass like password)
        }.let {
            if (it.empty()) {
                null
            } else {
                it.first()[UsersTable.id]
            }
        }
    }
}

fun hasUser(userName: String) = Tables.transaction {
    UsersTable.selectAll().any {
        it[UsersTable.nick] == userName
    }
}

fun tryRegister(userName: String, password: String, name: String, surname: String, mail: String): Int? {
    if (hasUser(userName))
        return null
    try {
        Tables.transaction {
            val id = UsersTable.selectAll()
                .orderBy(UsersTable.id, SortOrder.DESC)
                .first()[UsersTable.id] + 1
            UsersTable.insert {
                it[UsersTable.id] = id
                it[nick] = userName
                it[UsersTable.name] = name
                it[UsersTable.surname] = surname
                it[UsersTable.mail] = mail
                it[pass] = password
            }
        }
    } catch (err: Throwable) {
        return null
    }
    return Tables.transaction {
        UsersTable.select {
            UsersTable.nick like userName
        }.let { q ->
            if (q.empty()) {
                null
            } else {
                q.first()[UsersTable.id]
            }
        }
    }
}

data class UserInfo(
    val id: Int,
    val nick: String,
    val name: String,
    val surname: String,
    val mail: String,
    val pass: String
) {
    constructor(row: ResultRow) : this(
        row[UsersTable.id],
        row[UsersTable.nick],
        row[UsersTable.name],
        row[UsersTable.surname],
        row[UsersTable.mail],
        row[UsersTable.pass]
    )

    val fullName: String
        get() {
            val buf = StringBuilder()
            buf.append(name)
            buf.append(' ').append(surname)
            val fullName = buf.toString().trim()
            return if (fullName.isEmpty()) nick else fullName
        }
}

fun getUser(userId: Int) = Tables.transaction {
    UsersTable.select {
        UsersTable.id eq userId
    }.let { q ->
        if (q.empty()) {
            null
        } else {
            q.first().let {
                UserInfo(
                    it[UsersTable.id],
                    it[UsersTable.nick],
                    it[UsersTable.name],
                    it[UsersTable.surname],
                    it[UsersTable.mail],
                    it[UsersTable.pass]
                )
            }
        }
    }
}

fun getUser(userName: String) = Tables.transaction {
    UsersTable.select {
        UsersTable.nick like userName
    }.let { q ->
        if (q.empty()) {
            null
        } else {
            UserInfo(q.first())
        }
    }
}

fun getUserNames() = Tables.transaction {
    UsersTable.selectAll()
        .map {
            it[UsersTable.nick]
        }
}

fun getUsers() = Tables.transaction {
    UsersTable.selectAll()
        .map {
            UserInfo(it)
        }
}

fun removeUser(userInfo: UserInfo) = Tables.transaction {
    UsersTable.deleteWhere {
        (UsersTable.id eq userInfo.id)
    }
}


data class SearchFileResult(
    val userName: String,
    val user: String,
    val fileId: String,
    val fileName: String,
    val fileSize: FileSize
)
fun searchFiles(pattern: String) = Tables.transaction {
    FilesInfoTable
        .innerJoin(UsersTable, {FilesInfoTable.userId}, {UsersTable.id})
        /*.select {
            FilesInfoTable.name like pattern
        }*/
        .selectAll()
        .filter {
            it[FilesInfoTable.name].contains(pattern, ignoreCase = true)
        }
        .map {
            val user = UserInfo(it)
            SearchFileResult(
                user.fullName,
                user.nick,
                it[FilesInfoTable.id],
                it[FilesInfoTable.name],
                FileSize(it[FilesInfoTable.size])
            )
        }
}


fun updateUserInfo(userId: Int, name: String, surname: String) = Tables.transaction {
    UsersTable.update(
        where = { UsersTable.id eq userId }
    ) {
        it[UsersTable.name] = name
        it[UsersTable.surname] = surname
    }
}



fun getBugs() = Tables.transaction {
    BugsTable.selectAll()
        .map {
            it.toBug()
        }
}

fun ResultRow.toBug() = Bug(
    this[BugsTable.id],
    this[BugsTable.title],
    this[BugsTable.content],
    try {
        BugStatus.values()[this[BugsTable.status]]
    } catch (err: Throwable) {
        BugStatus.NEW
    },
    getUser(this[BugsTable.owner])!!
)

fun getBug(bugId: Int) = Tables.transaction {
    BugsTable.select {
        BugsTable.id eq bugId
    }.let {
        if (it.empty())
            null
        else
            it.first().toBug()
    }
}

fun newBug(title: String, content: String, owner: Int) = Tables.transaction {
    val id = BugsTable.selectAll()
        .orderBy(BugsTable.id, SortOrder.DESC)
        .let { q ->
            if (q.empty())
                1
            else
                q.first().let {
                    it[BugsTable.id] + 1
                }
        }
    BugsTable.insert {
        it[BugsTable.id] = id
        it[BugsTable.title] = title
        it[BugsTable.content] = content
        it[BugsTable.status] = BugStatus.NEW.ordinal
        it[BugsTable.owner] = owner
    }
    Bug(id, title, content, BugStatus.NEW, getUser(owner)!!)
}

/*fun setBugStatus(bugId: Int, status: BugStatus) = Tables.transaction {
    BugsTable.update(
        where = { BugsTable.id eq bugId }
    ) {
        it[BugsTable.status] = status.ordinal
    }
}

fun setBugContent(bugId: Int, content: String) = Tables.transaction {
    BugsTable.update(
        where = { BugsTable.id eq bugId }
    ) {
        it[BugsTable.content] = content
    }
}*/
