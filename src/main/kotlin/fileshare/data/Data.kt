package fileshare.data

import org.jetbrains.exposed.sql.*
import org.sqlite.SQLiteException
import java.io.File
import java.io.IOException
import java.sql.Connection
import java.sql.SQLException

object Tables {
    fun <T> transaction(statement: Transaction.() -> T) =
        org.jetbrains.exposed.sql.transactions
            .transaction(Connection.TRANSACTION_SERIALIZABLE, 1, null, statement)

    fun connect() = try {
        val wd = File("").absoluteFile
        val db = File(wd, "data/db")
        if (!db.exists()) {
            if (!db.mkdirs())
                throw IOException("cannot create database")
        }
        Database.connect("jdbc:sqlite:data/db/data.sdb", driver = "org.sqlite.JDBC")
        true
    } catch (err: Throwable) {
        false
    }

    fun create() {
        transaction {
            SchemaUtils.create(
                UsersTable,
                FilesTable,
                FilesInfoTable,
                BugsTable
            )
        }
    }

    fun isEmpty(): Boolean {
        return try {
            transaction { UsersTable.selectAll().empty() }
        } catch (err: Throwable) {
            return true
        }
    }

    fun fill() {
        transaction {
            UsersTable.insert {
                it[id] = 1
                it[nick] = "admin"
                it[name] = "Administrator"
                it[surname] = ""
                it[mail] = "egor.chalyh.98@mail.ru"
                it[pass] = "4fba3bc02b72ee9a687a1e5286e373c6"
            }
        }
    }

    fun drop() {
        transaction {
            SchemaUtils.drop(
                FilesInfoTable,
                FilesTable,
                UsersTable,
                BugsTable
            )
        }
    }

}
