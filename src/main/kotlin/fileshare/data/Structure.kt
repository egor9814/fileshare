package fileshare.data

import org.jetbrains.exposed.sql.Table

object UsersTable : Table("a") {
    val id = integer("a").primaryKey().uniqueIndex()
    val nick = varchar("b", 255)
    val name = varchar("c", 255)
    val surname = varchar("d", 255)
    val mail = varchar("e", 255)
    val pass = varchar("f", 32)
}

object FilesTable : Table("b") {
    val id = varchar("a", 255).primaryKey()
    val data = blob("b")
}

object FilesInfoTable : Table("c") {
    val id = varchar("a", 255).primaryKey().references(FilesTable.id)
    val name = varchar("b", 255)
    val userId = integer("c").references(UsersTable.id)
    val creationTimestamp = long("d")
    val size = long("e")
    val type = varchar("f", 255)
}

object BugsTable : Table("d") {
    val id = integer("a").primaryKey()
    val title = varchar("b", 255)
    val content = varchar("c", 65000)
    val status = integer("d")
    val owner = integer("e").references(UsersTable.id)
}
