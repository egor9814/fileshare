package fileshare.data

enum class BugStatus(val iconName: String) {
    NEW("new_releases"),
    REJECTED("cancel"),
    ACCEPTED("access_time"),
    FIXED("done")
}

data class Bug(
    val id: Int,
    val title: String,
    val content: String,
    val status: BugStatus,
    val owner: UserInfo
) {
    fun send(): String {
        return "mailto:egor9814-original@yandex.ru?subject=$title&body=$content"
    }
}
