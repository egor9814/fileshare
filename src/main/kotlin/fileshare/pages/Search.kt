package fileshare.pages

import fileshare.FSSession
import fileshare.data.searchFiles
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.content.PartData
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import util.cssStyle
import util.styleCss

/*private object SearchGet : AbstractPage("/search/{Query?}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        call.respondHtml {
            defaultHead("FileShare - Search") {}
            body {
                titleBar {
                    backIcon()
                    title("Search")
                    homeIcon()
                }
                card {
                    div {
                        form("/search/", encType = FormEncType.multipartFormData, method = FormMethod.get) {
                            p {
                                label {
                                    +"File name pattern: "
                                    textInput {
                                        name = "query"
                                    }
                                }
                                submitInput { value = "Go!" }
                            }
                        }
                    }
                }
            }
        }
    }
}*/

private object SearchPost : AbstractPage("/search") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        debug("getting query...")
        val query = call.request.queryParameters["q"] ?: ""
        debug("query = \"$query\"")

        call.respondHtml {
            defaultHead("FileShare - Search \"$query\"") {}
            body {
                titleBar {
                    backIcon()
                    title("Search")
                    homeIcon()
                }
                card {
                    div {
                        form("/search", method = FormMethod.get) {
                            p {
                                label {
                                    +"File name pattern: "
                                    textInput {
                                        name = "q"
                                        value = query
                                    }
                                }
                                submitInput { value = "Go!" }
                            }
                        }
                    }
                    styleCss {
                        rule(".row") {
                            width = LinearDimension("100%")
                        }
                    }
                    if (query.isNotEmpty()) {
                        div {
                            attributes.cssStyle {
                                width = LinearDimension("inherit")
                            }
                            //debug("query result begin")
                            val files = searchFiles(query)
                            //files.forEach { debug(it) }
                            //debug("query result end")
                            if (files.isEmpty()) {
                                p { +"Not found!" }
                            } else {
                                table {
                                    attributes["border"] = "1"
                                    attributes.cssStyle {
                                        width = LinearDimension("inherit")
                                    }
                                    tr {
                                        th { +"File name" }
                                        th { +"Size" }
                                        th { +"User" }
                                    }
                                    for (f in files) {
                                        tr {
                                            td { a(href = "/file/${f.fileId}") {+f.fileName} }
                                            td { +f.fileSize.toString() }
                                            td { a(href = "/user/${f.user}") {+f.userName} }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


object Search : PageRegistrar(/*SearchGet, */SearchPost)
