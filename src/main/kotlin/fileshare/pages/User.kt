package fileshare.pages

import fileshare.AccessMode
import fileshare.FSSession
import fileshare.data.getUser
import fileshare.data.updateUserInfo
import fileshare.getAccessMode
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.content.PartData
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import util.cssStyle
import util.styleCss

private object UserGet : AbstractPage("/user/{UserName}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val userName = call.parameters["UserName"]
        call.respondHtml {
            defaultHead("FileShare - User $userName") {}
            body {
                titleBar {
                    backIcon()
                    title(userName ?: "")
                    homeIcon()
                }
                card {
                    if (userName == null) {
                        p { +"Invalid user" }
                    } else {
                        val user = getUser(userName)
                        if (user == null) {
                            p { +"User $userName not exists" }
                        } else {
                            val canEdit = user.id == session.userId
                            form("/user/update", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                                table {
                                    attributes["border"] = "1"
                                    attributes["cellspacing"] = "1"
                                    attributes.cssStyle {
                                        width = LinearDimension("inherit")
                                        padding(2.px)
                                    }
                                    tr {
                                        td { +"Login" }
                                        td { +user.nick }
                                    }
                                    tr {
                                        td { +"Name" }
                                        td {
                                            if (canEdit) {
                                                textInput {
                                                    name = "name"
                                                    value = user.name
                                                }
                                            } else {
                                                +user.name
                                            }
                                        }
                                    }
                                    tr {
                                        td { +"Surname" }
                                        td {
                                            if (canEdit) {
                                                textInput {
                                                    name = "surname"
                                                    value = user.surname
                                                }
                                            } else {
                                                +user.surname
                                            }
                                        }
                                    }
                                    tr {
                                        td { +"E-Mail" }
                                        td { +user.mail }
                                    }
                                }
                                if (canEdit) {
                                    submitInput { value = "Apply!" }
                                }
                            }
                            p {
                                a(href = "/user/$userName/files") { +"Show files" }
                            }
                        }
                    }
                }
            }
        }
    }
}


private object UserPost : AbstractPage("/user/update", RouteMethod.POST) {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val parts = call.receiveMultipart()
        var name = ""
        var surname = ""
        while (true) {
            val part = parts.readPart() ?: break
            if (part is PartData.FormItem) {
                when (part.name) {
                    "name" -> name = part.value
                    "surname" -> surname = part.value
                }
            }
            part.dispose()
        }
        updateUserInfo(session.userId!!, name, surname)
        val user = getUser(session.userId)!!
        call.sessions.set(session.copy(name = user.fullName))
        call.respondRedirect("/user/${user.nick}")
    }
}


object User : PageRegistrar(UserGet, UserPost)
