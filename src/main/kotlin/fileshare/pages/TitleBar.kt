package fileshare.pages

import kotlinx.css.*
import kotlinx.css.Float
import kotlinx.css.properties.LineHeight
import kotlinx.css.properties.boxShadow
import kotlinx.html.*
import util.cssStyle

class TitleBar internal constructor(
    private val textColor: Color
) {
    enum class Gravity {
        Start, End
    }

    fun FlowContent.icon(name: String = "add", page: String? = null, gravity: Gravity = Gravity.Start) {
        div {
            attributes.cssStyle {
                width = 32.px
                height = 32.px
                margin(4.px, 4.px, 4.px, 4.px)
                float = if (gravity == Gravity.Start) Float.left else Float.right
            }
            a(href = page) {
                i(classes = "material-icons") {
                    attributes.cssStyle {
                        color = textColor
                        textAlign = TextAlign.center
                        lineHeight = LineHeight("32px")
                    }
                    +name
                }
            }
        }
    }

    fun FlowContent.backIcon() = icon("navigate_before", "javascript:history.back()")

    fun FlowContent.homeIcon() = icon("home", "/", Gravity.End)

    fun FlowContent.addBugIcon() = icon("add", "/bug/new", Gravity.End)

    fun FlowContent.bugIcon() = icon("bug_report", "/bugs", Gravity.End)

    fun FlowContent.title(value: String = "") {
        div {
            attributes.cssStyle {
                height = 32.px
                lineHeight = LineHeight("32px")
                margin(4.px, 4.px, 4.px, 4.px)
                float = Float.left
                fontSize = 24.px
                fontWeight = FontWeight.bold
                textAlign = TextAlign.left
                verticalAlign = VerticalAlign.top
                display = Display.contents
            }
            +value
        }
    }
}

fun FlowContent.titleBar(
    textColor: Color = Color("#FFFFFF"),
    bgColor: Color = Color("#3F51B5"),
    drawShadow: Boolean = true,
    block: TitleBar.() -> Unit = {}
) {
    val tb = TitleBar(textColor)
    div {
        attributes.cssStyle {
            width = LinearDimension("100%")
            height = 40.px
            backgroundColor = bgColor
            color = textColor
            padding(0.px, 4.px, 0.px, 4.px)
            if (drawShadow)
                boxShadow(Color.black.withAlpha(0.2), blurRadius = 6.px)
        }
        block(tb)
    }
}
