package fileshare.pages

import fileshare.FSSession
import fileshare.InvalidCredentialsException
import fileshare.data.getUser
import fileshare.data.tryLogin
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.*
import io.ktor.http.content.PartData
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import util.LoginManager
import util.PasswordManager
import util.cssStyle

private object LoginGet : AbstractPage("/login") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        if (LoginManager.isLogined(session))
            call.respondRedirect("/")

        call.respondHtml {
            defaultHead("Login to FileShare") {}
            body {
                titleBar {
                    backIcon()
                    title("Login")
                    homeIcon()
                }
                card {
                    p { +"Fill fields" }
                    form("/login", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                        attributes.cssStyle {
                            borderStyle = BorderStyle.solid
                            borderWidth = 2.px
                            borderColor = Color.gray
                            padding(8.px)
                        }
                        table {
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "login"
                                        +"Login:"
                                    }
                                }
                                td {
                                    textInput {
                                        name = "login"
                                        id = "login"
                                        required = true
                                    }
                                }
                            }
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "password"
                                        +"Password:"
                                    }
                                }
                                td {
                                    passwordInput {
                                        name = "password"
                                        id = "password"
                                        required = true
                                    }
                                }
                            }
                        }
                        submitInput { value = "Sign In" }
                    }
                }
            }
        }
    }
}


private object LoginPost : AbstractPage("/login", RouteMethod.POST) {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val parts = call.receiveMultipart()
        var login = ""
        var password = ""
        while (true) {
            val part = parts.readPart() ?: break
            if (part is PartData.FormItem) {
                when (part.name) {
                    "login" -> login = part.value
                    "password" -> password = part.value
                }
            }
            part.dispose()
        }
        if (password.isEmpty() || login.isEmpty()) {
            throw InvalidCredentialsException("Login or password is empty.", 0)
        } else {
            val pass = PasswordManager.encode(password)
            val userId = tryLogin(login, pass)
            if (userId == null) {
                throw InvalidCredentialsException("Cannot login to $login. Login not found, or password not matching.", 0)
            } else {
                val user = getUser(userId)!!
                call.sessions.set(LoginManager.login(session, user))
                call.respondRedirect("/")
            }
        }
    }
}


object Login : PageRegistrar(LoginGet, LoginPost)
