package fileshare.pages

import fileshare.Debugger
import fileshare.FSSession
import fileshare.getSession
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.response.respondRedirect
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.util.pipeline.PipelineContext
import util.LoginManager

object Logout : AbstractPage("/logout") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val s = getSession()
        call.sessions.set(s.copy(name = null, userId = null, salt = null))
        call.respondRedirect("/")
    }

    override fun checkSessionHook(session: FSSession) {}
}
