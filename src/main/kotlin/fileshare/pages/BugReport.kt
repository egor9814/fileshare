package fileshare.pages

import fileshare.FSSession
import fileshare.InternalServerException
import fileshare.InvalidCredentialsException
import fileshare.data.*
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.content.PartData
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import fileshare.data.Bug
import util.LoginManager
import util.cssStyle

private object BugView : AbstractPage("/bug/view/{BugId}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val bugId = call.parameters["BugId"]?.toInt() ?: -1

        call.respondHtml {
            defaultHead("FileShare - Bug Report") {}
            body {
                titleBar {
                    backIcon()
                    title(if (bugId == -1) "Invalid bug number '$bugId'" else "Bug #$bugId")
                    bugIcon()
                    homeIcon()
                }
                card {
                    val bug = getBug(bugId)
                    if (bug == null) {
                        h2 { +"Bug is solved" }
                    } else {
                        table {
                            tr {
                                td {
                                    titleBar(
                                        textColor = Color.black,
                                        bgColor = Color.transparent,
                                        drawShadow = false
                                    ) {
                                        icon(bug.status.iconName)
                                        title(bug.title)
                                        /*if (session.userId == bug.owner.id) {
                                            icon("send", "javascript:window.open(\"${bug.send()}\")", TitleBar.Gravity.End)
                                        }*/
                                    }
                                }
                            }
                            tr {
                                td {
                                    p {
                                        attributes.cssStyle {
                                            margin(0.px, 0.px, 0.px, 48.px)
                                            color = Color.gray
                                        }
                                        +"by "
                                        a(href = "/user/${bug.owner.nick}") {
                                            attributes.cssStyle {
                                                color = Color.inherit
                                            }
                                            +bug.owner.fullName
                                        }
                                    }
                                }
                            }
                            tr {
                                td {
                                    div {
                                        attributes.cssStyle {
                                            margin(0.px, 8.px, 0.px, 8.px)
                                        }
                                        p { +bug.content }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


private object BugsList : AbstractPage("/bugs") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        call.respondHtml {
            defaultHead("FileShare - Bug list") {}
            body {
                titleBar {
                    backIcon()
                    title("Bugs")
                    homeIcon()
                    if (LoginManager.isLogined(session))
                        addBugIcon()
                }
                card {
                    val bugs = getBugs()
                    when (bugs.size) {
                        0 -> p { +"No bugs!" }
                        1 -> shortPreview(bugs.first())
                        else -> {
                            table {
                                attributes.cssStyle {
                                    borderCollapse = BorderCollapse.collapse
                                    width = 100.pct
                                }
                                tr {
                                    td {
                                        shortPreview(bugs.first())
                                    }
                                }
                                for (bug in bugs.drop(1).dropLast(1)) {
                                    tr {
                                        attributes.cssStyle {
                                            borderTop = "solid"
                                            borderBottom = "solid"
                                            borderWidth = 1.px
                                            borderColor = Color.gray
                                        }
                                        td {
                                            shortPreview(bug)
                                        }
                                    }
                                }
                                tr {
                                    td {
                                        shortPreview(bugs.last())
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun FlowContent.divider() {
        div {
            attributes.cssStyle {
                height = 1.px
                width = 100.pct
                margin(1.px, 4.px, 1.px, 0.px)
                backgroundColor = Color.gray
            }
        }
    }

    private fun FlowContent.shortPreview(bug: Bug) {
        titleBar(
            textColor = Color.black,
            bgColor = Color.transparent,
            drawShadow = false
        ) {
            icon(bug.status.iconName)
            title(bug.title)
            icon("pageview", "/bug/view/${bug.id}", TitleBar.Gravity.End)
        }
    }
}


private object BugNewGet : AbstractPage("/bug/new") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        if (!LoginManager.isLogined(session)) {
            throw InvalidCredentialsException("Only logined users may create bug report!", 1)
        }

        call.respondHtml {
            defaultHead("FileShare - New bug") {}
            body {
                titleBar {
                    backIcon()
                    title("Creating bug report")
                    bugIcon()
                    homeIcon()
                }
                card {
                    form("/bug/new", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                        attributes.cssStyle {
                            borderStyle = BorderStyle.solid
                            borderWidth = 2.px
                            borderColor = Color.gray
                            padding(8.px)
                        }
                        table {
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "title"
                                        +"Title: "
                                    }
                                }
                                td {
                                    textInput {
                                        id = "title"
                                        name = "title"
                                        maxLength = "255"
                                        required = true
                                    }
                                }
                            }
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "content"
                                        +"Content: "
                                    }
                                }
                                td {
                                    textArea {
                                        id = "content"
                                        name = "content"
                                        maxLength = "65000"
                                        required = true
                                    }
                                }
                            }
                        }
                        p {
                            submitInput { value = "Create" }
                        }
                    }
                }
            }
        }
    }
}


private object BugNewPost : AbstractPage("/bug/new", RouteMethod.POST) {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val parts = call.receiveMultipart()
        var title = ""
        var content = ""
        while (true) {
            val part = parts.readPart() ?: break
            if (part is PartData.FormItem) {
                when (part.name) {
                    "title" -> title = part.value
                    "content" -> content = part.value
                }
            }
            part.dispose()
        }
        if (content.isEmpty() || title.isEmpty()) {
            throw InternalServerException(RuntimeException("Title or content is empty."))
        } else {
            val newBug = newBug(title, content, session.userId!!)
            call.respondRedirect("/bug/view/${newBug.id}")
        }
    }
}


object BugReport : PageRegistrar(BugView, BugsList, BugNewGet, BugNewPost)
