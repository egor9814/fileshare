package fileshare.pages

import fileshare.FSSession
import fileshare.InternalServerException
import fileshare.data.getFiles
import fileshare.data.getUser
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.LinearDimension
import kotlinx.html.*
import util.cssStyle

object Files : AbstractPage("/user/{UserName}/files") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val userName = call.parameters["UserName"]
        val userInfo = getUser(userName ?: throw InternalServerException(RuntimeException("User $userName not exists")))
            ?: throw InternalServerException(RuntimeException("User $userName not exists"))

        call.respondHtml {
            val prefix = if (session.userId == userInfo.id) "My " else ""
            defaultHead("FileShare - ${prefix}Files") {}
            body {
                titleBar {
                    backIcon()
                    title("File list")
                    homeIcon()
                }
                card {
                    if (session.userId == userInfo.id) {
                        p { a(href = "/upload") { +"Upload file" } }
                    }
                    val files = getFiles(userInfo.id)
                    if (files.isEmpty()) {
                        p { +"Empty!" }
                    } else {
                        table {
                            attributes["border"] = "1"
                            attributes.cssStyle {
                                width = LinearDimension("inherit")
                            }
                            tr {
                                th { +"Name" }
                                th { +"Size" }
                            }
                            for (f in files) {
                                tr {
                                    td { a(href = "/file/${f.id}") { +f.name } }
                                    td { +f.size.toString() }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
