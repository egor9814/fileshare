package fileshare.pages

import fileshare.*
import fileshare.data.createFileInfo
import fileshare.data.getUser
import fileshare.data.pushFile
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.*
import io.ktor.http.content.PartData
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import util.cssStyle

private object UploadGet : AbstractPage("/upload") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        call.respondHtml {
            defaultHead("FileShare - Uploading") {}
            body {
                titleBar {
                    backIcon()
                    title("Uploading file")
                    homeIcon()
                }
                card {
                    p { +"Fill fields" }
                    form("/upload", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                        attributes.cssStyle {
                            borderStyle = BorderStyle.solid
                            borderWidth = 2.px
                            borderColor = Color.gray
                            padding(8.px)
                        }
                        p {
                            label {
                                +"File:  "
                                fileInput {
                                    name = "file"
                                    id = "file"
                                    onChange = "uploadFile()"
                                }
                            }
                        }
                        progress {
                            attributes.cssStyle {
                                width = 100.pct
                            }
                            id = "progress_bar"
                            value = "0"
                            max = "100"
                        }
                        h3 {
                            id = "status"
                        }
                        p {
                            id = "loaded_n_total"
                        }
                        /*p {
                            submitInput { value = "Upload!" }
                        }*/
                    }
                }
                script(src = "/static/upload.js") {}
            }
        }
    }
}


private object UploadPost : AbstractPage("/upload", RouteMethod.POST) {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val parts = call.receiveMultipart()
        var file = ""
        val out = mutableListOf<Byte>()
        while (true) {
            val part = parts.readPart() ?: break
            if (part is PartData.FileItem) {
                when (part.name) {
                    "file" -> {
                        file = part.originalFileName ?: ""
                        val input = part.provider()
                        val buf = ByteArray(1024)
                        while (!input.endOfInput) {
                            val read = input.readAvailable(buf, 0, 1024)
                            for (i in 0 until read) {
                                out.add(buf[i])
                            }
                        }
                        input.close()
                    }
                }
            }
            part.dispose()
        }
        if (file.isEmpty())
            throw InternalServerException(RuntimeException("cannot upload file: empty name"))

        val info = createFileInfo(session.userId!!, file, out)
        pushFile(info, ExposedBlob(out.toByteArray()))

        call.respondText("/user/${getUser(session.userId)!!.nick}/files")
    }
}


object Upload : PageRegistrar(UploadGet, UploadPost)
