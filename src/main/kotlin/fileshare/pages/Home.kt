package fileshare.pages

import fileshare.FSSession
import fileshare.data.getUser
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.css.Float
import kotlinx.css.properties.TextDecoration
import kotlinx.html.*
import util.styleCss

object Home : AbstractPage("/") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        call.respondHtml {
            defaultHead("FileShare") {}
            body {
                styleCss {
                    rule("#navigation_menu, a") {
                        padding(left = 10.px, right = 10.px)
                        textDecoration = TextDecoration.none
                    }
                    rule("#navigation_menu1") {
                        //textAlign = TextAlign.start
                        float = Float.left
                    }
                    rule("#navigation_menu2") {
                        //textAlign = TextAlign.end
                        float = Float.right
                    }
                    rule("#navigation_menu") {
                        backgroundColor = Color("#EEEEEE")
                    }
                }
                titleBar {
                    title(session.name ?: "Welcome to FileShare")
                    //title("Welcome to FileShare${session.name?.let { ", $it" } ?: ""}")
                    bugIcon()
                }
                card {
                    div {
                        id = "navigation_menu"
                        val userName = if (session.userId != null) {
                            getUser(session.userId)?.nick ?: ""
                        } else {
                            ""
                        }
                        div {
                            id = "navigation_menu1"
                            table {
                                attributes["border"] = "1"
                                tr {
                                    if (session.userId != null) {
                                        td {
                                            a(href = "/user/$userName/files") { +"My Files" }
                                        }
                                    }
                                    td {
                                        a(href = "/users") { +"Users" }
                                    }
                                    td {
                                        a(href = "/search") { +"Search" }
                                    }
                                }
                            }
                        }
                        div {
                            id = "navigation_menu2"
                            table {
                                attributes["border"] = "1"
                                tr {
                                    if (session.userId == null) {
                                        td {
                                            a(href = "/login") { +"Sign In" }
                                        }
                                        td {
                                            a(href = "/register") { +"Sign Up" }
                                        }
                                    } else {
                                        td {
                                            a(href = "/logout") { +"Log out" }
                                        }
                                        td {
                                            a(href = "/user/$userName") { +"Settings" }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
