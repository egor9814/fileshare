package fileshare.pages

import fileshare.FSSession
import fileshare.InternalServerException
import fileshare.InvalidCredentialsException
import fileshare.data.getUser
import fileshare.data.removeUser
import fileshare.data.tryRegister
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.*
import io.ktor.http.content.PartData
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.BorderStyle
import kotlinx.css.Color
import kotlinx.css.padding
import kotlinx.css.px
import kotlinx.html.*
import util.LoginManager
import util.MD5
import util.PasswordManager
import util.cssStyle

private object RegisterGet : AbstractPage("/register") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        if (LoginManager.isLogined(session))
            call.respondRedirect("/")

        call.respondHtml {
            defaultHead("Register to FileShare") {}
            body {
                titleBar {
                    backIcon()
                    title("Register")
                    homeIcon()
                }
                card {
                    p { +"Fill fields" }
                    form("/register", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                        attributes.cssStyle {
                            borderStyle = BorderStyle.solid
                            borderWidth = 2.px
                            borderColor = Color.gray
                            padding(8.px)
                        }
                        table {
                            //attributes["border"] = "1"
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "login"
                                        +"Login: "
                                    }
                                }
                                td {
                                    textInput {
                                        name = "login"
                                        id = "login"
                                        required = true
                                    }
                                }
                            }
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "password"
                                        +"Password: "
                                    }
                                }
                                td {
                                    passwordInput {
                                        name = "password"
                                        id = "password"
                                        required = true
                                    }
                                }
                            }
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "mail"
                                        +"E-Mail: "
                                    }
                                }
                                td {
                                    emailInput {
                                        name = "mail"
                                        id = "mail"
                                        required = true
                                    }
                                }
                            }
                        }
                        p {
                            submitInput { value = "Sign Up" }
                        }
                    }
                }
            }
        }
    }
}


private object RegisterPost : AbstractPage("/register", RouteMethod.POST) {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val parts = call.receiveMultipart()
        var login = ""
        var password = ""
        var mail = ""
        while (true) {
            val part = parts.readPart() ?: break
            if (part is PartData.FormItem) {
                when (part.name) {
                    "login" -> login = part.value
                    "password" -> password = part.value
                    "mail" -> mail = part.value
                }
            }
            part.dispose()
        }
        when {
            password.isEmpty() -> throw InvalidCredentialsException("Password is empty.", 1)
            login.isEmpty() -> throw InvalidCredentialsException("Login is empty.", 1)
            mail.isEmpty() -> throw InvalidCredentialsException("E-Mail is empty.", 1)
            else -> {
                val pass = PasswordManager.encode(password)
                //val data = MD5(MD5(pass) + MD5(login) + MD5(mail))
                //val token = MD5(LoginManager.sendRegisterToken(mail, data))
                val userId = tryRegister(login, pass, "", "", mail)
                if (userId != null) {
                    //call.respondRedirect("/register/apply/$token$userId")
                    val user = getUser(userId)!!
                    call.sessions.set(LoginManager.login(session, user))
                    call.respondRedirect("/")
                } else {
                    throw InvalidCredentialsException("Cannot register new user $login, maybe user is already registered.", 1)
                }
            }
        }
    }
}

/*private object RegisterTokenGet : AbstractPage("/register/apply/{Token}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        if (LoginManager.isLogined(session))
            call.respondRedirect("/")

        val tokenData = call.parameters["Token"] ?: throw InvalidCredentialsException("Cannot register invalid user", 1)

        call.respondHtml {
            defaultHead("Register to FileShare") {}
            body {
                titleBar {
                    backIcon()
                    title("Register")
                    homeIcon()
                }
                card {
                    p { +"Fill fields" }
                    form("/register/apply/$tokenData", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                        attributes.cssStyle {
                            borderStyle = BorderStyle.solid
                            borderWidth = 2.px
                            borderColor = Color.gray
                            padding(8.px)
                        }
                        table {
                            tr {
                                td {
                                    label {
                                        attributes["for"] = "token"
                                        +"Enter token from E-Mail: "
                                    }
                                }
                                td {
                                    textInput {
                                        name = "token"
                                        id = "token"
                                    }
                                }
                            }
                        }
                        p {
                            submitInput { value = "Check!" }
                        }
                    }
                }
            }
        }
    }
}


private object RegisterTokenPost : AbstractPage("/register/apply/{Token}", RouteMethod.POST) {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val tokenData = call.parameters["Token"] ?: throw InvalidCredentialsException("Cannot register invalid user", 1)
        val expectedToken = tokenData.substring(0, 32)
        val userId = tokenData.substring(32).toInt()

        val parts = call.receiveMultipart()
        var token = ""
        while (true) {
            val part = parts.readPart() ?: break
            if (part is PartData.FormItem) {
                when (part.name) {
                    "token" -> token = part.value
                }
            }
            part.dispose()
        }
        when {
            token.isEmpty() -> throw InvalidCredentialsException("Token is empty.", 1)
            else -> {
                try {
                    val data = MD5(token)
                    if (data != expectedToken)
                        throw RuntimeException("Tokens from E-Mail is invalid!")
                    val user = getUser(userId)!!
                    call.sessions.set(LoginManager.login(session, user))
                    call.respondRedirect("/")
                } catch (err: Throwable) {
                    getUser(userId)?.let(::removeUser)
                    throw InternalServerException(err)
                }
            }
        }
    }
}*/


object Register : PageRegistrar(RegisterGet, RegisterPost/*, RegisterTokenGet, RegisterTokenPost*/)
