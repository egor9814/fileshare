package fileshare.pages

import fileshare.FSSession
import fileshare.data.getUsers
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import util.cssStyle
import util.styleCss

object Users : AbstractPage("/users") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        call.respondHtml {
            defaultHead("FileShare - User list") {}
            body {
                titleBar {
                    backIcon()
                    title("Users")
                    homeIcon()
                }
                styleCss {
                    rule("#users_table, tr, td") {
                        padding(2.px)
                    }
                }
                card {
                    val users = getUsers()
                    if (users.isEmpty()) {
                        p { +"Empty!" }
                    } else {
                        table {
                            id = "users_table"
                            attributes["border"] = "1"
                            attributes["cellspacing"] = "1"
                            attributes.cssStyle {
                                width = LinearDimension("inherit")
                            }
                            tr {
                                th { +"Name" }
                                th { +"E-Mail" }
                            }
                            for (u in users) {
                                tr {
                                    td { a(href = "/user/${u.nick}"){+u.fullName} }
                                    td { +u.mail }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
