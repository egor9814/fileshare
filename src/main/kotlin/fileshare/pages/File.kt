package fileshare.pages

import fileshare.FSSession
import fileshare.InternalServerException
import fileshare.data.getFileBlob
import fileshare.data.getFileInfo
import fileshare.data.getUser
import fileshare.data.removeFile
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.response.respondFile
import io.ktor.response.respondRedirect
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.css.LinearDimension
import kotlinx.html.*
import util.LoginManager
import util.cssStyle

private object File : AbstractPage("/file/{FileID}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val fileId = call.parameters["FileID"] ?: throw InternalServerException(RuntimeException("invalid file id"))
        val fileInfo = getFileInfo(fileId)
        call.respondHtml {
            val titlePrefix = fileInfo?.name ?: "Not Exists"
            defaultHead("FileShare - File $titlePrefix") {}
            body {
                titleBar {
                    backIcon()
                    title(titlePrefix)
                    icon("home", "/", TitleBar.Gravity.End)
                }
                card {
                    if (fileInfo != null) {
                        table {
                            attributes["border"] = "1"
                            attributes.cssStyle {
                                width = LinearDimension("inherit")
                            }
                            tr {
                                td { +"Name" }
                                td { +fileInfo.name }
                            }
                            tr {
                                td { +"Size" }
                                td { +fileInfo.size.toString() }
                            }
                            tr {
                                td { +"Owner" }
                                val user = getUser(fileInfo.userId)!!
                                td { a(href = "/user/${user.nick}") { +user.fullName } }
                            }
                        }
                        p {
                            a(href = "/download/$fileId/${fileInfo.name}") { +"Download" }
                        }
                        if (LoginManager.isLogined(session) && session.userId == fileInfo.userId) {
                            p {
                                a(href = "/delete/$fileId") { +"Delete file" }
                            }
                        }
                    }
                }
            }
        }
    }
}


private object FileDownload : AbstractPage("/download/{FileID}/{FileName?}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val fileId = call.parameters["FileID"] ?: throw InternalServerException(RuntimeException("invalid file id"))
        val fileName = call.parameters["FileName"] ?: ""
        val fileInfo = getFileInfo(fileId)
        if (fileInfo == null) {
            call.respondHtml {
                defaultHead("FileShare - Downloading error") {}
                body {
                    titleBar {
                        backIcon()
                        title("Downloading error")
                        homeIcon()
                    }
                    card {
                        p { +"File not exists!" }
                    }
                }
            }
        } else {
            try {
                require(fileName == fileInfo.name)
                val wd = java.io.File("").absoluteFile
                val tmpDir = java.io.File(wd, "data/files/tmp/${fileInfo.id}/")
                if (!tmpDir.exists()) {
                    if (!tmpDir.mkdirs())
                        throw RuntimeException("cannot get server dir")
                }
                val tmpFile = java.io.File(tmpDir, fileInfo.name)
                if (!tmpFile.exists()) {
                    if (!withContext(Dispatchers.IO) { tmpFile.createNewFile() }) {
                        throw RuntimeException("cannot get server file")
                    }
                }
                val blob = getFileBlob(fileInfo.id)
                withContext(Dispatchers.IO) {
                    tmpFile.outputStream().write(blob.bytes)
                }
                call.respondFile(tmpFile)
            } catch (err: Throwable) {
                throw InternalServerException(err)
            }
        }
    }
}


private object FileRemove : AbstractPage("/delete/{FileID}") {
    override suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession) {
        val fileId = call.parameters["FileID"] ?: throw InternalServerException(RuntimeException("invalid file id"))
        val fileInfo = getFileInfo(fileId)
        if (fileInfo == null) {
            call.respondHtml {
                defaultHead("FileShare - Deleting error") {}
                body {
                    titleBar {
                        backIcon()
                        title("Deleting error")
                        homeIcon()
                    }
                    card {
                        p { +"File not exists!" }
                    }
                }
            }
        } else {
            try {
                require(session.userId == fileInfo.userId)
                removeFile(fileInfo)
                val wd = java.io.File("").absoluteFile
                val tmpDir = java.io.File(wd, "data/files/tmp/${fileInfo.id}/")
                val tmpFile = java.io.File(tmpDir, fileInfo.name)
                if (tmpFile.exists()) {
                    tmpFile.delete()
                    tmpDir.delete()
                }
                call.respondRedirect("/user/${getUser(session.userId)!!.nick}/files")
            } catch (err: Throwable) {
                throw InternalServerException(err)
            }
        }
    }
}


object FilePages : PageRegistrar(File, FileDownload, FileRemove)
