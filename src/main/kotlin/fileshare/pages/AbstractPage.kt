package fileshare.pages

import fileshare.Debugger
import fileshare.FSSession
import fileshare.getSession
import io.ktor.application.ApplicationCall
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.BoxSizing
import kotlinx.html.*
import util.LoginManager
import util.styleCss

enum class RouteMethod {
    GET,
    POST
}
abstract class AbstractPage(val path: String, val routeMethod: RouteMethod = RouteMethod.GET) {
    protected abstract suspend fun PipelineContext<Unit, ApplicationCall>.applyRouting(session: FSSession)

    private var _d: Debugger? = null

    operator fun get(debugger: Debugger) = apply {
        _d = debugger
    }

    protected fun <T> debug(value: T?) = apply {
        _d?.println("$path> $value")
    }

    protected fun debug() = apply {
        _d?.println("$path>")
    }

    operator fun invoke(routing: () -> Routing) = apply {
        register(routing())
    }

    protected open fun checkSessionHook(session: FSSession) {
        try {
            LoginManager.checkUser(session)
        } catch (err: Throwable) {
            debug(err.toString())
            throw err
        }
    }

    fun register(routing: Routing) = apply {
        if (routeMethod == RouteMethod.GET) {
            routing.get(path) {
                getSession().let {
                    val user = if (it.userId == null) "" else ": ${it.name}"
                    debug("GET$user")
                    checkSessionHook(it)
                    applyRouting(it)
                }
            }
        } else {
            routing.post(path) {
                getSession().let {
                    val user = if (it.userId == null) "" else ": ${it.name}"
                    debug("POST$user")
                    checkSessionHook(it)
                    applyRouting(it)
                }
            }
        }
    }

    /*protected fun BODY.loadJS() {
        script(src = "/static/kotlin.js") {}
        script(src = "/static/fileshare.js") {}
    }*/
}

fun HTML.defaultHead(title: String, block: HEAD.() -> Unit) {
    head {
        title(title)
        meta {
            httpEquiv = "X-UA-Compatible"
            content = "IE=edge"
        }
        meta {
            name = "viewport"
            content = "width=device-width, initial-scale=1.0"
        }
        link(href = "https://fonts.googleapis.com/icon?family=Material+Icons", rel = "stylesheet")
        styleCss {
            rule("*") {
                boxSizing = BoxSizing.borderBox
            }
            kotlinx.css.body {
                margin = "0"
            }
        }
        block()
    }
}


abstract class PageRegistrar(vararg val pages: AbstractPage) {
    operator fun invoke(routing: () -> Routing) = apply {
        pages.forEach { it.invoke(routing) }
    }

    operator fun get(debugger: Debugger) = apply {
        pages.forEach { it[debugger] }
    }
}
