package fileshare.pages

import kotlinx.css.*
import kotlinx.css.properties.boxShadow
import kotlinx.html.*
import util.*

class CardView internal constructor()

fun FlowContent.card(
    shadowColor: Color = Color.black,
    bgColor: Color = Color.white,
    block: CardView.() -> Unit = {}
) {
    val p = CardView()
    div {
        attributes.cssStyle {
            margin(16.px, 8.px, 16.px, 8.px)
            boxShadow(shadowColor.withAlpha(0.2), 0.px, 4.px, 8.px, 0.px)
            boxShadow(shadowColor.withAlpha(0.19), 0.px, 6.px, 20.px, 0.px)
            backgroundColor = bgColor
            padding(8.px)
            width = LinearDimension("-webkit-fill-available")
            height = LinearDimension.maxContent
            position = Position.fixed
        }
        block(p)
    }
}
